package me.pixka.incom.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestAddCustomer {

    @Autowired
    lateinit var cs: CustomerService

    fun addCustomer(n: String, tel: String = "0894297443"): Customer {
        return cs.save(Customer(n, tel))
    }

    @Test
    fun testAdd() {
        addCustomer("customer 1")
        addCustomer("customer 2")

        Assertions.assertTrue(cs.all().size > 0)
    }

    @Test
    fun testSearch() {
        addCustomer("customer 1")
        addCustomer("customer 2")
        var list = cs.search("", 0, 1000)
        Assertions.assertTrue(list!!.size > 0)

    }

    @Test
    fun testEdit() {
        var editcustomer = addCustomer("test edit Customer")

        var foredit = cs.find(editcustomer.id)

        foredit?.name = "XXXX"
        foredit?.tel = "0000"
        foredit = cs.save(foredit!!)


        var forcheck = cs.find(editcustomer.id)
        Assertions.assertTrue(forcheck?.name.equals("XXXX"))
        Assertions.assertTrue(forcheck?.tel.equals("0000"))

    }
}