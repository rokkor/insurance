package me.pixka.incom.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.math.BigDecimal

@DataJpaTest
class TestInsurance {
    @Autowired
    lateinit var iss: InsuranceService

    @Autowired
    lateinit var its: InsureruntypeService
    @Autowired
    lateinit var cs:CustomerService


    fun add(n: String, insuracetype: Insureruntype): Insurance {

        var t = Insurance()
        t.name = n
        t.insureruntype = insuracetype
        return iss.save(t)
    }

    @Test
    fun TestAdd() {
        var t = its.findOrCreate("type 1", BigDecimal(100))

        var i = add("Test insurance", t)

        var list = iss.all()
        Assertions.assertTrue(list.size>0)

    }
}