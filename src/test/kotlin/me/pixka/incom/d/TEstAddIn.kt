package me.pixka.incom.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TEstAddIn {

    @Autowired
    lateinit var iss:InsureruntypeService


    fun add(n:String): Insureruntype {
        return iss.save(Insureruntype(n))
    }
    @Test
    fun testAdd()
    {
        add("in test 1")
        Assertions.assertTrue(iss.all().size>0)
    }
    @Test
    fun testSearchtype()
    {
        add("forsearch 1")
        add("forsearch 2")
        var list = iss.search("",0,1000)
        Assertions.assertTrue(list!!.size>0)
    }
}