package me.pixka.incom.c

import me.pixka.base.o.SearchOption
import me.pixka.incom.d.CustomerService
import me.pixka.incom.d.Insurance
import me.pixka.incom.d.InsuranceService
import me.pixka.incom.d.InsureruntypeService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
class InsurnceControl(val iss: InsuranceService, val cs: CustomerService, val ist: InsureruntypeService) {

    @PostMapping("/insurance/add")
    @CrossOrigin
    fun add(@RequestBody insurence: Insurance): Insurance {
        try {
            insurence.customer = cs.findOrCreate(insurence.customer!!.name!!)
            if(insurence.insureruntype?.comp == null)
                insurence.insureruntype?.comp = BigDecimal(0)
            insurence.insureruntype =
                ist.findOrCreate(insurence.insureruntype!!.name!!)
            return iss.save(insurence)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }
    @PostMapping("insurance/sn")
    @CrossOrigin
    fun sn(@RequestBody search:SearchOption): List<Insurance>? {
        try{
           return  iss.search(search.search!!,search.page!!,search.limit!!)
        }
        catch (e:Exception)
        {
            e.printStackTrace()
            throw  e
        }
    }

}