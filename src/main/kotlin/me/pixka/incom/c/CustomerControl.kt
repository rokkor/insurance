package me.pixka.incom.c

import me.pixka.base.o.SearchOption
import me.pixka.incom.d.Customer
import me.pixka.incom.d.CustomerService
import org.springframework.web.bind.annotation.*

@RestController
class CustomerControl(val cs: CustomerService) {
    @PostMapping("/customer/add")
    @CrossOrigin
    fun add(@RequestBody customer: Customer): Customer {
        try {
            return cs.save(customer)
        } catch (e: Exception) {
            throw e
        }
    }

    @PostMapping("/customer/sn")
    @CrossOrigin
    fun add(@RequestBody s: SearchOption): List<Customer>? {
        try {
            return cs.search(s.search!!, s.page!!, s.limit!!)
        } catch (e: Exception) {
            throw e
        }
    }


    @PostMapping("/customer/edit")
    @CrossOrigin
    fun edit(@RequestBody customer: Customer): Customer? {
        try {

            return cs.save(customer)

        } catch (e: Exception) {
            throw e
        }
    }

    @GetMapping("/customer/get/{id}")
    @CrossOrigin
    fun add(@PathVariable("id") id: Long): Customer? {
        try {
            var cus = this.cs.find(id)
            return cus

        } catch (e: Exception) {
            e.printStackTrace()
            throw  e
        }
    }
}