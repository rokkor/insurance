package me.pixka.incom.c

import me.pixka.base.o.SearchOption
import me.pixka.incom.d.Insureruntype
import me.pixka.incom.d.InsureruntypeService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class InsuranceTypeControl(val its: InsureruntypeService) {

    @PostMapping("/insurancetype/add")
    @CrossOrigin
    fun addtype(@RequestBody() data: Insureruntype): Insureruntype {
        try {
            return its.findOrCreate(data.name!!,data.comp!!)
        } catch (e: Exception) {
            throw e
        }
    }

    @PostMapping("/insurancetype/list")
    @CrossOrigin
    fun sntype(@RequestBody se: SearchOption): List<Insureruntype>? {
        try {
            var re =  its.search(se!!.search!!, se!!.page!!, se!!.limit!!)
            return re
        } catch (e: Exception) {
            throw e
        }
    }
}