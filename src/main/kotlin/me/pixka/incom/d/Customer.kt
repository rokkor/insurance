package me.pixka.incom.d

import me.pixka.base.d.En
import me.pixka.base.s.DefaultService
import me.pixka.base.s.findByName
import me.pixka.base.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.Entity

@Entity
class Customer(var name: String? = null, var tel: String? = null,var address:String?=null) : En()
{
    override fun toString(): String {
        return "id: ${id} name:${name} tel:${tel} ver:${ver}"
    }
}

@Repository
interface CustomerRepo : JpaRepository<Customer, Long>, search<Customer>, findByName<Customer> {
    @Query("from Customer c where c.name like %?1% or c.tel like %?1%")
    override fun search(s: String, page: Pageable): List<Customer>?
}

@Service
class CustomerService() : DefaultService<Customer>() {


    @Synchronized
    fun findOrCreate(n:String,tel:String?=null): Customer {
        var cc = findByName(n)
        if (cc == null)
            return save(Customer(n,tel))

        return cc
    }
}