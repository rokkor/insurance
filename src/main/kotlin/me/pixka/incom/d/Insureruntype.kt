package me.pixka.incom.d

import me.pixka.base.d.En
import me.pixka.base.s.DefaultService
import me.pixka.base.s.findByName
import me.pixka.base.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import javax.persistence.Entity

@Entity
class Insureruntype(
    var name: String? = null, var mainpay: BigDecimal? = null, var secpay: BigDecimal? = null,
    var comp: BigDecimal? = null,var description:String?=null
) : En() {
}

@Repository
interface InsureruntypeRepo:JpaRepository<Insureruntype,Long>,search<Insureruntype>,findByName<Insureruntype>
{
    @Query("from Insureruntype i where i.name like %?1%")
    override fun search(s: String, page: Pageable): List<Insureruntype>?

}


@Service
class InsureruntypeService:DefaultService<Insureruntype>()
{
    @Synchronized
    fun findOrCreate(n:String,c:BigDecimal=BigDecimal(0)): Insureruntype {
        var found = findByName(n)
        if(found==null)
        {
            var n  = Insureruntype(n)
            n.comp = c
            return save(n)
        }
        return found
    }
}