package me.pixka.incom.d

import me.pixka.base.d.En
import me.pixka.base.s.DefaultService
import me.pixka.base.s.findByName
import me.pixka.base.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
class Insurance(
    var name: String? = null, @ManyToOne var customer: Customer? = null,
    @Column(insertable = false, updatable = false) var customer_id: Long? = null,
    @ManyToOne var insureruntype: Insureruntype? = null, @Column(insertable = false, updatable = false)
    var insureruntype_id: Long? = null, var pay1: BigDecimal? = null, var pay2: BigDecimal? = null,
    var idate: Date? = null
) : En() {
}

@Repository
interface InsuranceRepo : JpaRepository<Insurance, Long>, search<Insurance>, findByName<Insurance> {
    @Query("from Insurance i  where i.customer.name like %?1%")
    override fun search(s: String, page: Pageable): List<Insurance>?
}

@Service
class InsuranceService : DefaultService<Insurance>()

