package me.pixka.incom

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = arrayOf("me.pixka"))
class IncomApplication

fun main(args: Array<String>) {
	runApplication<IncomApplication>(*args)
}
